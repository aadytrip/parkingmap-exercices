<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parks extends Model
{
	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = 'parks';
}
