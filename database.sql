CREATE DATABASE parkingmap_exercices;

USE parkingmap_exercices;

CREATE TABLE parkings(
   id int(11) UNSIGNED NOT NULL,
   name varchar(255) NOT NULL,
   latitude double(10, 6) NOT NULL,
   longitude double(10, 6) NOT NULL,
   CONSTRAINT pk_parkings PRIMARY KEY(id)
);

CREATE TABLE parking_spaces(
   id int(11) UNSIGNED NOT NULL,
   parking_id int(11) UNSIGNED NOT NULL,
   CONSTRAINT pk_parking_spaces PRIMARY KEY(id),
   CONSTRAINT parking_spaces_parking_id_foreign FOREIGN KEY(parking_id) REFERENCES parkings(id)
);

CREATE TABLE parks(
   id int(11) UNSIGNED NOT NULL,
   parking_space_id int(11) UNSIGNED NOT NULL,
   parkin datetime NOT NULL,
   parkout datetime DEFAULT NULL,
   CONSTRAINT pk_parks PRIMARY KEY(id),
   CONSTRAINT fk_parks_parking_space FOREIGN KEY(parking_space_id) REFERENCES parking_spaces(id)
);

INSERT INTO parkings(id, name, latitude, longitude) VALUES(123, 'Carrefour ABC', 1.234568, 1.234568), (234, 'Auchan XYZ', 2.345679, 2.345679);

INSERT INTO parking_spaces(id, parking_id) VALUES(123, 123), (234, 123), (345, 123), (456, 234), (567, 234), (678, 234);

INSERT INTO parks(id, parking_space_id, parkin, parkout) VALUES(1, 123, '2017-08-08 16:01:00', '2017-08-08 16:09:00'), (2, 123, '2017-08-08 16:02:00', '2017-08-08 16:05:00'), (3, 123, '2017-08-08 16:03:00', '2017-08-08 16:30:00'), (4, 234, '2017-08-08 17:01:00', '2017-08-08 17:09:00'), (5, 234, '2017-08-08 17:02:00', '2017-08-08 17:05:00'), (6, 234, '2017-08-08 19:03:00', '2017-08-08 19:30:00'), (7, 345, '2017-08-08 19:04:00', NULL);