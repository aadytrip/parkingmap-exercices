# Exercices pour ParkingMap

Les exercices ont été réalisés en utilisant Laravel et MySQL.

## Fichiers utiles

À la racine de ce dépot ce trouve le fichier *database.sql* qui contient les requêtes pour créer la base de données (nommée *parkingmap_exercices*), trois tables (*parkings*, *parking_spaces*, *parks*) et quelques insertions.

Dans le dossier *resources/views* se trouve le fichier *home.blade.php* qui affiche les résultats des exercices.

Les traitements sont effectués dans le fichier *routes/web.php*.

## Utilisation

- Cloner ce dépot `git clone https://gitlab.com/aadytrip/parkingmap-exercices.git`
- Lancer lancer commande `composer install`
- Pour créer la base de données, vous avez deux possibilités : soit lancer les migrations avec la commande `php artisan migrate` ou bien importer la base de données depuis le fichier *database.sql*.
- Lancer le serveur web avec la commande `php artisan serve`

## Quelques explications

### Exercice 1

L'exercice est d'afficer le nombre de places total.

La réponse se trouve dans la table *parking_spaces*, qui stocke les places de parkings. Le nombre de ligne dans la table est le nombre de place total.

```sql
SELECT COUNT(*) as count FROM parking_spaces;
```

### Exercice 2

L'exercice est d'afficer le nombre d’entrées et de sorties, heure par heure.

La réponse se trouve dans la table *parks* qui stocke les entrées et sorties des véhicules. Nous utilisons la fonction *HOUR* sur le champ *parkin* pour extraire la partie heure de l'entrée du véhicule. Nous groupons ensuite sur cette valeur et puis comptons le nombre de lignes groupé.

```sql
SELECT parking_space_id, HOUR(parkin) AS hour, COUNT(*) AS count
FROM parks
GROUP BY parking_space_id, HOUR(parkin)
```

Nous faisons la même chose avec le champ *parkout* qui est la datetime de sortie du véhicule. Nous ajoutons en plus une vérification que la valeur ne soit pas nulle, puis qu'il se peut qu'un véhicule soit entré mais ne soit pas encore sortie.

```sql
SELECT parking_space_id, HOUR(parkout) AS hour, COUNT(*) AS count
FROM parks
WHERE parkout IS NOT NULL
GROUP BY parking_space_id, HOUR(parkout)
```
