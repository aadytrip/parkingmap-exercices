<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Exercices | ParkingMap</title>
	</head>
	<body>
		<h1>ParkingMap</h1>
		
		<section>
			<h2>Exercice 1</h2>
			<p>Le nombre de places total : {{ $data['parkingSpaces']->count }}</p>
		</section>
		
		<section>
			<h2>Exercice 2</h2>
			
			<p>Le nombre d'entrées</p>
			<table border="1">
				<thead>
					<tr>
						<th>parking_space_id</th>
						<th>hour</th>
						<th>parkins</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data['parkinsByHour'] as $parkin)
					<tr>
						<td>{{ $parkin->parking_space_id }}</td>
						<td>{{ $parkin->hour }}</td>
						<td>{{ $parkin->count }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
			<p>Le nombre de sorties</p>
			<table border="1">
				<thead>
					<tr>
						<th>parking_space_id</th>
						<th>hour</th>
						<th>parkouts</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data['parkoutsByHour'] as $parkout)
					<tr>
						<td>{{ $parkout->parking_space_id }}</td>
						<td>{{ $parkout->hour }}</td>
						<td>{{ $parkout->count }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</section>
	</body>
</html>