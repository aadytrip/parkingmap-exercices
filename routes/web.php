<?php

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
	$parkingSpaces = DB::selectOne(
		DB::raw('SELECT COUNT(*) AS count FROM parking_spaces')
	);

	$parkinsByHour = DB::select(
		DB::raw('
			SELECT parking_space_id, HOUR(parkin) AS hour, COUNT(*) AS count
			FROM parks
			GROUP BY parking_space_id, HOUR(parkin)
		')
	);

	$parkoutsByHour = DB::select(
		DB::raw('
			SELECT parking_space_id, HOUR(parkout) AS hour, COUNT(*) AS count
			FROM parks
			WHERE parkout IS NOT NULL
			GROUP BY parking_space_id, HOUR(parkout)
		')
	);

	$data = [
		'parkingSpaces' => $parkingSpaces,
		'parkinsByHour' => $parkinsByHour,
		'parkoutsByHour' => $parkoutsByHour,
	];

	return view('home', ['data' => $data]);
});
