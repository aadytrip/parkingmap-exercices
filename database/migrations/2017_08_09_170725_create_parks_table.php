<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // parks(id, parking_space_id, datetime_in, datetime_out) # parking_space_id faisant référence à 'id' de la table 'parking_spaces'.
        Schema::create('parks', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('parking_space_id')->unsigned();
            $table->dateTime('parkin');
            $table->dateTime('parkout')->nullable();
            $table->primary('id');
            $table->foreign('parking_space_id')->references('id')->on('parking_spaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parks');
    }
}
