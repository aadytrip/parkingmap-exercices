<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertParkingsIntoParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('parkings')->insert(
            array(
                array('id' => 123, 'name' => 'Carrefour ABC', 'latitude' => 1.23456789, 'longitude' => 1.23456789),
                array('id' => 234, 'name' => 'Auchan XYZ', 'latitude' => 2.34567891, 'longitude' => 2.34567891),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('parkings')->delete();
    }
}
