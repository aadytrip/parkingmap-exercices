<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertParksIntoParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // parks(id, parking_space_id, datetime_in, datetime_out) # parking_space_id faisant référence à 'id' de la table 'parking_spaces'.
        DB::table('parks')->insert(
            array(
                array('id' => 1, 'parking_space_id' => 123, 'parkin' => '2017-8-8 16:01:00', 'parkout' => '2017-8-8 16:09:00'),
                array('id' => 2, 'parking_space_id' => 123, 'parkin' => '2017-8-8 16:02:00', 'parkout' => '2017-8-8 16:05:00'),
                array('id' => 3, 'parking_space_id' => 123, 'parkin' => '2017-8-8 16:03:00', 'parkout' => '2017-8-8 16:30:00'),
                array('id' => 4, 'parking_space_id' => 234, 'parkin' => '2017-8-8 17:01:00', 'parkout' => '2017-8-8 17:09:00'),
                array('id' => 5, 'parking_space_id' => 234, 'parkin' => '2017-8-8 17:02:00', 'parkout' => '2017-8-8 17:05:00'),
                array('id' => 6, 'parking_space_id' => 234, 'parkin' => '2017-8-8 19:03:00', 'parkout' => '2017-8-8 19:30:00'),
                array('id' => 7, 'parking_space_id' => 345, 'parkin' => '2017-8-8 19:04:00', 'parkout' => null),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('parks')->delete();
    }
}
