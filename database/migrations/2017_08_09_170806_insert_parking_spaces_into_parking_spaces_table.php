<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertParkingSpacesIntoParkingSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('parking_spaces')->insert(
            array(
                array('id' => 123, 'parking_id' => 123),
                array('id' => 234, 'parking_id' => 123),
                array('id' => 345, 'parking_id' => 123),
                array('id' => 456, 'parking_id' => 234),
                array('id' => 567, 'parking_id' => 234),
                array('id' => 678, 'parking_id' => 234),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('parking_spaces')->delete();
    }
}
